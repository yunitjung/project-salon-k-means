<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimoni_model extends CI_Model
{
    private $table = 'testimoni';
    private $index = 'id_testimoni';

    public function __construct()
    {
        parent::__construct();
    }

    public function view_testimoni()
    {
        $this->db->select('*')
                ->from('testimoni t')
                ->join('pemesanan p', 'p.id_pemesanan = t.id_pemesanan')
                ->join('user u', 'u.username = t.username');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function view_testimoni_for_user($username)
    {
        $this->db->select('*')
                ->from('testimoni t')
                ->join('pemesanan p', 'p.id_pemesanan = t.id_pemesanan')
                ->join('user u', 'u.username = t.username')
                ->where('u.username', $username);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function view_testimoni_detail($username, $id_pemesanan)
    {
        $this->db->select('*')
                ->from('testimoni t')
                ->join('pemesanan p', 'p.id_pemesanan = t.id_pemesanan')
                ->join('user u', 'u.username = t.username')
                ->where('u.username', $username)
                ->where('p.id_pemesanan', $id_pemesanan);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function insert_testimoni($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function delete_testimoni($id)
    {
        $this->db->delete($this->table, array($this->index => $id));
    }


}