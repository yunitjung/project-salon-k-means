<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggan_model extends CI_Model
{

    private $table = 'user';
    private $index = 'username';

    public function __construct()
    {
        parent::__construct();
    }

    public function view_user()
    {
        $data = $this->db->get_where($this->table, array('is_admin' => 2));
        return $data->result_array();
    }

    public function view_user_by_id($id)
    {
        $data = $this->db->get_where($this->table, array($this->index => $id));
        return $data->row();
    }

    public function insert_user($data)
    {
        $this->db->insert($this->table, $data);
        return $data['username'];
    }

    public function update_user($id, $data)
    {
        $this->db->trans_start();

        $this->db
                ->where(array($this->index => $id))
                ->update($this->table, $data);
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE)
        {
            $response = array(
                'message' => "Unable to edit",
                'status' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $response = array(
                    'message' => "Edited successfully",
                    'status' => true
                );
            }
            else
            {
                $response = array(
                    'message' => "Edited successfully, but it looks like you haven't updated anything!",
                    'status' => true
                );
            }
        }
        return $response;
    }

    public function delete_user($id)
    {
        $this->db->delete($this->table, array($this->index => $id));
    }
}