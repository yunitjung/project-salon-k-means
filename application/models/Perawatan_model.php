<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perawatan_model extends CI_Model
{
    private $table = 'perawatan';
    private $index = 'id_perawatan';

    public function __construct()
    {
        parent::__construct();
    }

    public function view_perawatan()
    {
        $data = $this->db->get($this->table);
        return $data->result_array();
    }

    public function view_perawatan_by_id($id)
    {
        $data = $this->db->get_where($this->table, array($this->index => $id));
        return $data->result_array();
    }

    public function insert_perawatan($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update_perawatan($id, $data)
    {
        // $this->db->update($this->table, $data, $id);
        // if ($this->db->affected_rows() > 0) {
        //     return array("status" => TRUE, "data" => $id);

        // } else {
        //     return array("status" => FALSE, "data" => $id);
        // }

        $this->db->trans_start();

        $this->db
                ->where(array("id_perawatan" => $id))
                ->update($this->table, $data);
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE)
        {
            $response = array(
                'message' => "Unable to edit",
                'status' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $response = array(
                    'message' => "Eedited successfully",
                    'status' => true
                );
            }
            else
            {
                $response = array(
                    'message' => "Edited successfully, but it looks like you haven't updated anything!",
                    'status' => true
                );
            }
        }
        return $response;
    }

    public function delete_perawatan($id)
    {
        $this->db->delete($this->table, array($this->index => $id));
    }

}