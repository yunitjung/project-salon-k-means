<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemesanan_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

    }

    function view_pemesanan()
    {
        $this->db->select('id_pemesanan, tanggal, name, total_durasi, no_hp, total_bayar, status')
                    ->from('pemesanan')
                    ->join('user', 'user.username = pemesanan.username')
                    ->order_by('id_pemesanan','desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function view_pemesanan_by_id($id)
    {
        $this->db->select('id_pemesanan, tanggal, name, total_durasi, no_hp, total_bayar, status')
                    ->from('pemesanan as p')
                    ->join('user', 'user.username = p.username')
                    ->where('p.id_pemesanan', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function view_pemesanan_by_user($username)
    {
        $this->db->select('p.id_pemesanan, tanggal, name, total_durasi, no_hp, total_bayar, status')
                    ->from('pemesanan as p')
                    ->join('user as u', 'u.username = p.username')
                    ->where('u.username', $username);
        $query = $this->db->get();
        return $query->result_array();
    }

    function view_detail_trx($id)
    {
        $this->db->select('*')
                ->from('detail_trx d')
                ->join('terapis t', 't.id_terapis=d.id_terapis', 'left')
                ->join('perawatan p', 'p.id_perawatan = d.id_perawatan', 'left')
                ->where('d.id_pemesanan', $id);
        $query = $this->db->get();
        if($query->num_rows() != 0)
        {
            return $query->result_array();
        }
        else {
            return false;
        }

    }

    function check_order($user)
    {
        $data = $this->db->get_where( 'pemesanan', array(
                                                'username' => $user,
                                                'status'=> '0'));
        return $data->row();
    }

    function insert_pemesanan($data)
    {
        $this->db->insert('pemesanan', $data);
        return $this->db->insert_id();
    }

    function update_pemesanan($id, $data)
    {
        $this->db->trans_start();

        $this->db
                ->where(array("id_pemesanan" => $id))
                ->update('pemesanan', $data);
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE)
        {
            $response = array(
                'message' => "Unable to edit",
                'status' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $response = array(
                    'message' => "Edited successfully",
                    'status' => true
                );
            }
            else
            {
                $response = array(
                    'message' => "Edited successfully, but it looks like you haven't updated anything!",
                    'status' => true
                );
            }
        }
        return $response;
    }

    function update_detail($id, $data)
    {
        $this->db->trans_start();

        $this->db
                ->where(array("id_detail_trx" => $id))
                ->update('detail_trx', $data);
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE)
        {
            $response = array(
                'message' => "Unable to edit",
                'status' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $response = array(
                    'message' => "Edited successfully",
                    'status' => true
                );
            }
            else
            {
                $response = array(
                    'message' => "Edited successfully, but it looks like you haven't updated anything!",
                    'status' => true
                );
            }
        }
        return $response;
    }

    function insert_detail_trx($data)
    {
        $this->db->insert('detail_trx', $data);
        return $this->db->insert_id();
    }

    function delete_detail_trx($id)
    {
        $this->db->delete('detail_trx', array('id_detail_trx' => $id));
    }

    function delete_pemesanan($id)
    {
        $this->db->delete('pemesanan', array('id_pemesanan' => $id));
    }

    public function check_testimoni($id)
    {
        $query = $this->db->get_where('testimoni', array('id_pemesanan' => $id));
        $result = array();
        if($query->num_rows() > 0)
        {
            return true;
        }
        else return false;
    }

    function cek_cluster()
    {
        $last_row=$this->db->select('cluster')
                        ->from('pemesanan as p')
                        ->join('user as u', 'p.username = u.username')
                        ->order_by('p.id_pemesanan',"desc")->limit(1)->get()->row();
        return $last_row;
    }

    function get_cluster()
    {
        $query = $this->db->select('u.username, count(u.username) as total_dtg, sum(p.total_bayar) as total_byr, u.cluster')
                        ->from('pemesanan as p')
                        ->join('user as u', 'p.username = u.username' )
                        ->group_by('username');
        $query = $this->db->get();
        if($query->num_rows() != 0)
        {
            return $query->result_array();
        }
        else {
            return false;
        }

    }

    function update_cluster($id, $data)
    {
        $this->db->trans_start();

        $this->db->where(array("username" => $id))
                ->update('user', $data);
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE)
        {
            $response = array(
                'message' => "Unable to edit",
                'status' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $response = array(
                    'message' => "Edited successfully",
                    'status' => true
                );
            }
            else
            {
                $response = array(
                    'message' => "Edited successfully, but it looks like you haven't updated anything!",
                    'status' => true
                );
            }
        }
        return $response;
    }
}