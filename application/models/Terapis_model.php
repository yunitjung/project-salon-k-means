<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terapis_model extends CI_Model
{

    private $table = 'terapis';
    private $index = 'id_terapis';

    public function __construct()
    {
        parent::__construct();
    }

    public function view_terapis()
    {
        $data = $this->db->get($this->table);
        return $data->result_array();
    }

    public function view_terapis_by_hour($jam, $terapis)
    {
        $data = $this->db->get_where($this->table, array('jam_selesai >=' => $jam, 'id_terapis' => $terapis));
        return $data->result_array();
    }

    public function view_terapis_by_id($id)
    {
        $data = $this->db->get_where($this->table, array($this->index => $id));
        return $data->result_array();
    }

    public function insert_terapis($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update_terapis($id, $data)
    {
        $this->db->trans_start();

        $this->db
                ->where(array("id_terapis" => $id))
                ->update($this->table, $data);
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE)
        {
            $response = array(
                'message' => "Unable to edit",
                'status' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $response = array(
                    'message' => "Edited successfully",
                    'status' => true
                );
            }
            else
            {
                $response = array(
                    'message' => "Edited successfully, but it looks like you haven't updated anything!",
                    'status' => true
                );
            }
        }
        return $response;
    }

    public function delete_terapis($id)
    {
        $this->db->delete($this->table, array($this->index => $id));
    }
}