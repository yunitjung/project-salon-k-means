<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template {

    var $ci;

    function __construct()
    {
        $this->ci =& get_instance();
    }

    function set($content_area, $value)
    {
        $this->template_data[$content_area] = $value;
    }

    function load($template = '', $name ='', $view = '' , $view_data = array(), $return = FALSE)
		{
			$this->CI =& get_instance();

			$this->set($name , $this->CI->load->view($view, $view_data, TRUE));
			$this->CI->load->view($template, $this->template_data);
		}
}