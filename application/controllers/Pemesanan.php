<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemesanan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pemesanan_model');
        $this->load->model('pelanggan_model');
        $this->load->model('perawatan_model');
        $this->load->model('terapis_model');
    }

    public function index()
    {
        $data['title'] = 'Pemesanan';
        $user = $this->session->userdata('roles');
        $username = $this->session->userdata('username');
        if($user == '1')
        {
            $data['pemesanan'] = $this->pemesanan_model->view_pemesanan();
            $data['user'] = $this->pelanggan_model->view_user();
            $template['content'] = $this->load->view('admin/kelola_pemesanan', $data, TRUE);
            $this->load->view('admin/template', $template);
        }
        elseif($user == '2') {
            $data['pemesanan'] = $this->pemesanan_model->view_pemesanan_by_user($username);
            $i = 0;
            foreach($data['pemesanan'] as $q)
            {
                if($this->pemesanan_model->check_testimoni($q['id_pemesanan']))
                {
                    $data['pemesanan'][$i]['status_testi'] = 1;
                }
                else $data['pemesanan'][$i]['status_testi']  = 0;
               $i++;
            }
            $template['content'] = $this->load->view('frontend/pemesanan', $data, TRUE);
            $this->load->view('frontend/template', $template);
        }
        else
        {
            redirect('login');
        }
    }

    public function pemesanan($username)
    {
        $data['title'] = 'Pemesanan';
        $data['pemesanan'] = $this->pemesanan_model->view_pemesanan_by_user($username);
        $template['content'] = $this->load->view('frontend/pemesanan', $data, TRUE);
        $this->load->view('frontend/template', $template);
    }

    public function form_detail($id)
    {
        $this->load->model('perawatan_model');
        $this->load->model('terapis_model');
        $user = $this->session->userdata('roles');

        $data['title'] = 'Tambah pesanan';
        $data['pemesanan'] = $this->pemesanan_model->view_pemesanan_by_id($id);
        $data['terapis'] = $this->terapis_model->view_terapis();
        $data['detail_trx'] = $this->pemesanan_model->view_detail_trx($id);
        $data['perawatan'] = $this->perawatan_model->view_perawatan();


        $data['status'] = '';
        if($this->uri->segment(4)!== ''){
            $data['status'] = $this->uri->segment(4);
        }

        if($user == '1')
        {
            $template['content'] = $this->load->view('admin/pilih_pesanan_opsi1', $data, TRUE);
            $this->load->view('admin/template', $template);
        }
        elseif($user == '2') {
            $template['content'] = $this->load->view('frontend/pilih_pesanan', $data, TRUE);
            $this->load->view('frontend/template', $template);
        }
        elseif($user == '0')
        {
            $template['content'] = $this->load->view('frontend/pilih_pesanan', $data, TRUE);
            $this->load->view('frontend/template_pembeli', $template);
        }
        else{
            $template['content'] = $this->load->view('frontend/pilih_pesanan', $data, TRUE);
            $this->load->view('frontend/template_pembeli', $template);
        }

    }

    public function insert_pemesanan()
    {
        $data['username'] = $this->input->post('username');
        $data['tanggal'] = $this->input->post('tanggal');

        $result = $this->pemesanan_model->insert_pemesanan($data);
        if($result)
        {
            redirect('pemesanan/form_detail/'.$result);
        }
        else {
            redirect('pemesanan');
        }
    }

    function pemesanan_pelanggan_side($username, $id_perawatan)
    {
        $data = $this->pemesanan_model->check_order($username);
        if(!empty($data))
        {
            $to_insert['id_pemesanan'] = $data->id_pemesanan;
            $to_insert['id_perawatan'] = $id_perawatan;
            $result = $this->pemesanan_model->insert_detail_trx($to_insert);
            if($result)
            {
                redirect('pemesanan/form_detail/'.$data->id_pemesanan);
            }
            else {
                redirect('perawatan');
            }
        }
        else {
            $data['id_pemesanan'] = '';
            $data['username'] = $username;
            $data['tanggal'] = date('Y-m-d');
            $result = $this->pemesanan_model->insert_pemesanan($data);
            $to_insert['id_pemesanan'] = $result;
            $to_insert['id_perawatan'] = $id_perawatan;
            $results = $this->pemesanan_model->insert_detail_trx($to_insert);
            if($results)
            {
                redirect('pemesanan/form_detail/'.$result);
            }
            else {
                redirect('perawatan');
            }
        }
    }


    public function insert_detail_trx()
    {
        $data['id_pemesanan'] = $this->input->post('id_pemesanan');
        $data['id_terapis'] = $this->input->post('id_terapis');
        $data['id_perawatan'] = $this->input->post('id_perawatan');
        $perawatan = $this->perawatan_model->view_perawatan_by_id($data['id_perawatan']);

        date_default_timezone_set("Asia/Bangkok");
        $time = date('H:i');
        $hour = $minute = 0;
        list($hour, $minute) = explode(':', $time);

        $hour+= floor($perawatan[0]['durasi']/60);
        $minute+= floor($perawatan[0]['durasi']%60);

        $result_time = sprintf("%d:%02d:%02d", $hour, $minute, 0);
        $result_terapis = $this->terapis_model->view_terapis_by_hour($result_time, $data['id_terapis']);

        if(empty($result_terapis))
        {
            redirect('pemesanan/form_detail/'.$data['id_pemesanan'].'/0');

        }
        else
        {
            $result = $this->pemesanan_model->insert_detail_trx($data);
            redirect('pemesanan/form_detail/'.$data['id_pemesanan'].'/1');
        }

    }

    function delete_detail_trx($id_pemesanan = 0, $id = 0)
    {
        $this->pemesanan_model->delete_detail_trx($id);
        redirect('pemesanan/form_detail/'.$id_pemesanan);
    }

    function update_pemesanan()
    {
        $id_pemesanan = $this->input->post('id_pemesanan');
        $data['total_durasi'] = $this->input->post('total_durasi');
        $data['total_bayar'] = $this->input->post('total_harga');
        if($this->session->userdata('roles') == '1')
            $data['status'] = '1';
        elseif($this->session->userdata('roles') == '2')
            $data['status'] = '2';

        $result = $this->pemesanan_model->update_pemesanan($id_pemesanan, $data);

        if($result['status'])
        {
            redirect('pemesanan/invoice/'.$id_pemesanan);
        }
        else {
            redirect('pemesanan/form_detail/'.$id_pemesanan);
        }
    }

    function ajax_update_pesanan()
    {
        $id_pemesanan = $this->input->post('id_pemesanan');
        $data['total_durasi'] = $this->input->post('total_durasi');
        $data['total_bayar'] = $this->input->post('total_harga');
        $data['status'] = '1';
        $this->session->unset_userdata('besar_diskon');
        $result = $this->pemesanan_model->update_pemesanan($id_pemesanan, $data);
        echo json_encode($result);
    }

    function ajax_update_detail()
    {
        $id = $this->input->post('id_detail_trx');
        $data['id_terapis'] = $this->input->post('id_terapis');
        $result = $this->pemesanan_model->update_detail($id, $data);
        echo json_encode($result);
    }

    function update_status($id)
    {
        $data['status'] = '1';
        $result = $this->pemesanan_model->update_pemesanan($id, $data);
        if($result['status'])
        {
            redirect('pemesanan');
        }
        else {
            redirect('pemesanan/form_detail/'.$id);
        }
    }

    function update_status_for_admin($id)
    {
        $data = $this->input->post();
        $data['status'] = '2';
        $result = $this->pemesanan_model->update_pemesanan($id, $data);
        if($result['status'])
        {
            redirect('pemesanan');
        }
        else {
            redirect('pemesanan/form_detail/'.$id);
        }
    }

    function delete_pemesanan($id)
    {
        $this->pemesanan_model->delete_pemesanan($id);
        redirect('pemesanan');
    }

    function invoice($id)
    {
        $this->load->model('perawatan_model');
        $this->load->model('terapis_model');
        $data['title'] = 'Invoice';
        $data['pemesanan'] = $this->pemesanan_model->view_pemesanan_by_id($id);
        $data['perawatan']= $this->perawatan_model->view_perawatan();
        $data['terapis'] = $this->terapis_model->view_terapis();
        $data['detail_trx'] = $this->pemesanan_model->view_detail_trx($id);


        $data['status'] = '';
        if($this->uri->segment(4)!== ''){
            $data['status'] = $this->uri->segment(4);
        }
        if($this->session->userdata('roles') == '1')
        {
            $template['content'] = $this->load->view('admin/pilih_pesanan_opsi1', $data, TRUE);
            $this->load->view('admin/template', $template);
        }
        elseif($this->session->userdata('roles') == '2') {
            $template['content'] = $this->load->view('frontend/pilih_pesanan', $data, TRUE);
            $this->load->view('frontend/template', $template);
        }
        elseif($this->session->userdata('roles') == '0') {
            $template['content'] = $this->load->view('frontend/pilih_pesanan', $data, TRUE);
            $this->load->view('frontend/template_pembeli', $template);
        }
        else{
            $template['content'] = $this->load->view('frontend/pilih_pesanan', $data, TRUE);
            $this->load->view('frontend/template_pembeli', $template);
        }

    }

    function check_voucher()
    {
        $user = $this->input->post('username');
        $voucher = $this->input->post('kode_voucher');
        $id_pemesanan = $this->input->post('id_pemesanan');

        $pelanggan = $this->pelanggan_model->view_user_by_id($user);

        if($pelanggan->cluster == 1 && $voucher == 'YEAYSTAR10')
        {
            $dt = array('besar_diskon' => 10);
            $dt_to_update = array('diskon_voucher' => 10);
            echo json_encode(array('besar_diskon' => 10));
        }
        elseif($pelanggan->cluster == 2 && $voucher == 'YEAYSTAR20')
        {
            echo json_encode(array('besar_diskon' => 20));
        }
        elseif($pelanggan->cluster == 3 && $voucher == 'YEAYSTAR30')
        {
            echo json_encode(array('besar_diskon' => 30));
        }
        else
        {
            echo json_encode(array('besar_diskon' => 0));
        }
    }
}
