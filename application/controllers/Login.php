<?php

class Login extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
    }

    function index()
    {
        $this->load->view('login');
    }

    function signup()
    {
        $this->load->view('signup');
    }

    function auth()
    {
        $u = $this->input->post('username');
        $p = $this->input->post('password');

        $validate = $this->login_model->validate($u, $p);
        if($validate->username)
        {
            $username = $validate->username;
            $name = $validate->name;
            $email = $validate->email;
            $roles = $validate->is_admin;
            $sessdata = array(
                'username'  => $username,
                'name'      => $name,
                'email'     => $email,
                'roles'     => $roles,
                'logged_in' => TRUE
            );
            $this->session->set_userdata($sessdata);

            if($roles == '1')
                redirect('admin');
            elseif($roles == '2')
                redirect('perawatan');
        }
        else {
            echo $this->session->set_flashdata('msg','Username atau Password Salah');
            redirect('login');
        }
    }

    function logout()
    {
        $roles = $this->session->userdata('roles');
        $this->session->sess_destroy();
        if($roles == '1')
            redirect('login');
        elseif($roles == '2')
            redirect('perawatan');
    }
}