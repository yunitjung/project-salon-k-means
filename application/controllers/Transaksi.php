<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Phpml\Clustering\KMeans;
use Phpml\Clustering\KMeans\Cluster;
class Transaksi extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pemesanan_model');
        $this->load->model('pelanggan_model');
    }

    public function index()
    {
        $data['title'] = 'Transaksi';

        // $user = $this->session->userdata('roles');
        // $username = $this->session->userdata('username');
        // if($user == '1')
        // {
        $kmeans = $this->pemesanan_model->get_cluster();
        $dt_kmeans = array();
        foreach($kmeans as $d)
        {

            if($d['cluster'] == 1)
            $d['kategori'] = 'Silver';
            elseif($d['cluster'] == 2)
            $d['kategori'] = 'Gold';
            elseif($d['cluster'] == 3)
            $d['kategori'] = 'Platinum';

            $dt_kmeans[] = $d;
        }
        $data['kmeans'] = $dt_kmeans;
        $data['user'] = $this->pelanggan_model->view_user();
        $template['content'] = $this->load->view('admin/transaksi', $data, TRUE);
        $this->load->view('admin/template', $template);
        // }
        // else
        // {
        //     redirect('login');
        // }
    }

    public function get_kmeans_result()
    {
        //check butuh clustering atau gak
        $cek = $this->pemesanan_model->cek_cluster();
        if($cek->cluster == 0)
        {
            //butuh di cluster dulu
            $pemesanan = $this->pemesanan_model->get_cluster();
            $raw_data = array();
            //bersihkan data jadi range
            foreach($pemesanan as $p)
            {
                if($p['total_byr'] >= 0 && $p['total_byr'] < 121000)
                {
                    $total_byr = 1;
                }
                elseif($p['total_byr'] >= 121000 && $p['total_byr'] < 301000)
                {
                    $total_byr = 2;
                }
                elseif($p['total_byr'] >= 301000)
                {
                    $total_byr = 3;
                }

                if($p['total_dtg'] >= 1 && $p['total_dtg'] < 3)
                {
                    $total_dtg = 1;
                }
                elseif($p['total_dtg'] >= 3 && $p['total_dtg'] < 5)
                {
                    $total_dtg = 2;
                }
                elseif($p['total_dtg'] >= 5)
                {
                    $total_dtg = 3;
                }

                $raw_data[$p['username']] = array($total_dtg, $total_byr);
            }

            $kmeans = new KMeans(3);

            $kmeans_result = $kmeans->optimumCluster($raw_data, 2);

            $dt_to_return = array();

            foreach($kmeans_result as $key => $val)
            {
                foreach($val as $k=>$v)
                {
                    $this->pemesanan_model->update_cluster($k, array('cluster' => $key+1));
                }
            }
            // exit;
            redirect('transaksi');


        }
        else
        {
            //gak butuh
            redirect('transaksi');
        }

    }
}