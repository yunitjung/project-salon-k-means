<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pelanggan_model');
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Data Pelanggan';
        $data['pelanggan'] = $this->pelanggan_model->view_user();
        $user = $this->session->userdata('roles');
        if($user == '1')
        {
            $template['content'] = $this->load->view('admin/data_pelanggan', $data, TRUE);
            $this->load->view('admin/template', $template);
        }
        else {
            redirect('login');
        }
    }

    public function insert_user()
    {
        $data['username'] = $this->input->post('username');
        $data['name'] = $this->input->post('name');
        $data['password'] = $this->input->post('password');
        $data['email'] = $this->input->post('email');
        $data['no_hp'] = $this->input->post('no_hp');
        $data['is_admin'] = 2;
        $returned_data = $this->pelanggan_model->insert_user($data);

        redirect('perawatan');
    }

    public function delete_user($id)
    {
        $this->pelanggan_model->delete_user($id);
        redirect('pelanggan');
    }

    public function form_update($id)
    {
        $data = array();
        $data['pelanggan'] =  $this->pelanggan_model->view_user_by_id($id);
        $user = $this->session->userdata('roles');
        if($user == '1')
        {
            $data['title'] = 'Update Pelanggan';
            $template['content'] = $this->load->view('admin/update_data_pelanggan', $data, TRUE);
            $this->load->view('admin/template', $template);
        }
        elseif($user == '2') {
            $data['title'] = 'Kelola Profil';
            $template['content'] = $this->load->view('frontend/kelola_profil', $data, TRUE);
            $this->load->view('frontend/template', $template);
        }
    }

    public function update_pelanggan()
    {
        $id =  $this->input->post('username');
        $form_data['name'] = $this->input->post('name');
        $form_data['password'] = $this->input->post('password');
        $form_data['email'] = $this->input->post('email');
        $form_data['no_hp'] = $this->input->post('no_hp');

        $data = $this->pelanggan_model->update_user($id, $form_data);
        if($data['status'])
        {
            if($this->session->userdata('roles') == '1')
                redirect('pelanggan', $data['message']);
            else
                redirect('perawatan');
        }
        else {
            redirect('pelanggan/form_update/'.$id);
        }

    }

    public function form_data()
    {
        $data['title'] = 'Form Data Pembeli';
        $data['id_perawatan'] = $this->input->post('id_perawatan');
        $template['content'] =  $this->load->view('frontend/form_pembeli', $data, TRUE);
        $this->load->view('frontend/template_pembeli', $template);

    }

    public function create_tmp_pelanggan()
    {
        $this->load->model('pemesanan_model');
        $data['username'] = 'tmp_'.$this->input->post('name');
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['no_hp'] = $this->input->post('no_hp');
        $data['is_admin'] = '0';
        $id_perawatan = $this->input->post('id_perawatan');
        if(!$this->pelanggan_model->view_user_by_id($data['username']))
        {
            $username = $this->pelanggan_model->insert_user($data);
            $userdata = $this->pelanggan_model->view_user_by_id($username);
            $sessdata = array(
                'username'  => $userdata->username,
                'name'      => $userdata->name,
                'email'     => $userdata->email,
                'roles'     => $userdata->is_admin,
                'logged_in' => FALSE
            );
        }
        else {
            $username = $data['username'];
        }

        $data = $this->pemesanan_model->check_order($username);
        if($data)
        {
            $to_insert['id_pemesanan'] = $data->id_pemesanan;
            $to_insert['id_perawatan'] = $id_perawatan;
            $result = $this->pemesanan_model->insert_detail_trx($to_insert);
            if($result)
            {
                redirect('pemesanan/form_detail/'.$data->id_pemesanan);
            }
            else {
                redirect('perawatan');
            }
        }
        else {
            $data['id_pemesanan'] = '';
            $data['username'] = $username;
            $data['tanggal'] = date('Y-m-d');
            $result = $this->pemesanan_model->insert_pemesanan($data);
            $to_insert['id_pemesanan'] = $result;
            $to_insert['id_perawatan'] = $id_perawatan;
            $results = $this->pemesanan_model->insert_detail_trx($to_insert);
            if($results)
            {
                redirect('pemesanan/form_detail/'.$result);
            }
            else {
                redirect('perawatan');
            }
        }


    }

}
