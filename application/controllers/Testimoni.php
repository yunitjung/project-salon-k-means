<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimoni extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('testimoni_model');
        $this->load->model('pelanggan_model');
    }

    public function index()
    {
        $data['title'] = 'Testimoni';
        $user = $this->session->userdata('roles');
        $username = $this->session->userdata('username');

        if($user == '1')
        {
            $data['testimoni'] = $this->testimoni_model->view_testimoni();
            $template['content'] = $this->load->view('admin/testimoni', $data, TRUE);
            $this->load->view('admin/template', $template);
        }
    }

    public function tambah_testimoni($username, $id)
    {
        $data['title'] = 'Tambah Testimoni';
        $data['user'] = $this->pelanggan_model->view_user_by_id($username);
        $data['id_pemesanan'] = $id;
        $template['content']  = $this->load->view('frontend/testimoni.php', $data, TRUE);
        $this->load->view('frontend/template', $template);
    }

    public function insert_testimoni()
    {
        $data['username'] = $this->input->post('username');
        $data['id_pemesanan'] = $this->input->post('id_pemesanan');
        $data['kritik'] = $this->input->post('kritik');
        $data['saran'] = $this->input->post('saran');

        $result = $this->testimoni_model->insert_testimoni($data);

        if($result)
        {
            redirect('pemesanan');
        }
        else {
            redirect('testimoni/tambah_testimoni/'.$data['username'].'/'.$data['id_pemesanan']);
        }
    }

    public function delete_testimoni($id)
    {
        $this->testimoni_model->delete_testimoni($id);
        redirect('testimoni');
    }


}