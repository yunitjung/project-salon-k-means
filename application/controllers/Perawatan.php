<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perawatan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('perawatan_model');
    }

    public function index()
    {
        $data = array();
        $user = $this->session->userdata('roles');
        $data['title'] = 'Perawatan';
        $data['perawatan'] = $this->perawatan_model->view_perawatan();
        $data['user'] = $this->session->userdata('name');
        if($user)
        {
            $template['content'] = $this->load->view('frontend/perawatan', $data, TRUE);
            $this->load->view('frontend/template', $template);
        }
        else {
            $template['content'] = $this->load->view('frontend/perawatan_pembeli', $data, TRUE);
            $this->load->view('frontend/template_pembeli', $template);
        }
    }

    public function kelola_perawatan()
    {
        $data = array();
        $data['title'] = 'Kelola Perawatan';
        $user = $this->session->userdata('roles');
        $data['perawatan'] = $this->perawatan_model->view_perawatan();
        $data['roles'] = $this->session->userdata('roles');
        if($data['roles'] == '1')
        {
            $template['content'] = $this->load->view('admin/kelola_perawatan', $data, TRUE);
            $this->load->view('admin/template', $template);
        }
        else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function insert_perawatan()
    {
        $nama_perawatan = $this->input->post('nama_perawatan');
        $deskripsi = $this->input->post('deskripsi');
        $jenis_perawatan = $this->input->post('jenis_perawatan');
        $durasi = $this->input->post('durasi');
        $harga = $this->input->post('harga_perawatan');
        $promo = $this->input->post('promo');
        $config = array(
            'upload_path' => "./upload/",
            'allowed_types' => "gif|jpg|png|jpeg",
            'overwrite' => TRUE,
            'min_width' => '0',
            'min_height' => '0',
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'max_height' => "10000",
            'max_width' => "10000"
        );
        $this->load->library('upload', $config);
        $foto = 'upload/'.$_FILES['foto']['name'];
        $data = array(
            'nama_perawatan'    => $nama_perawatan,
            'deskripsi'         => $deskripsi,
            'jenis_perawatan'   => $jenis_perawatan,
            'durasi'            => $durasi,
            'harga'             => $harga,
            'promo'             => $promo,
            'foto'              => $foto
        );
        if($this->upload->do_upload('foto'))
        {
            $data = $this->perawatan_model->insert_perawatan($data);
            if($data)
                redirect('perawatan/kelola_perawatan');
            else {
                redirect('perawatan/kelola_perawatan');
            }
        }
        else {
            $error = array('error' => $this->upload->display_errors());
            var_dump($error);
        }
    }

    public function delete_perawatan($id)
    {
        $this->perawatan_model->delete_perawatan($id);
        redirect('perawatan/kelola_perawatan');

    }

    public function form_update($id)
    {
        $data = array();
        $data['title'] = 'Update Perawatan';
        $data['perawatan'] =  $this->perawatan_model->view_perawatan_by_id($id);
        $template['content'] = $this->load->view('admin/update_perawatan', $data, TRUE);
        $this->load->view('admin/template', $template);
    }

    public function update_perawatan()
    {
        $id_perawatan = $this->input->post('id_perawatan');
        $nama_perawatan = $this->input->post('nama_perawatan');
        $deskripsi = $this->input->post('deskripsi');
        $jenis_perawatan = $this->input->post('jenis_perawatan');
        $durasi = $this->input->post('durasi');
        $harga = $this->input->post('harga_perawatan');
        $promo = $this->input->post('promo');

        if($_FILES['foto']['name'] !== '')
        {
            $config = array(
                'upload_path' => "./upload/",
                'allowed_types' => "gif|jpg|png|jpeg",
                'overwrite' => TRUE,
                'min_width' => '0',
                'min_height' => '0',
                'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
                'max_height' => "10000",
                'max_width' => "10000"
            );
            $this->load->library('upload', $config);
            $tmp_foto = str_replace(' ', '_', $_FILES['foto']['name']);
            $foto = 'upload/'.$tmp_foto;
            $data = array(
                'nama_perawatan'    => $nama_perawatan,
                'deskripsi'         => $deskripsi,
                'jenis_perawatan'   => $jenis_perawatan,
                'durasi'            => $durasi,
                'harga'             => $harga,
                'promo'             => $promo,
                'foto'              => $foto
            );
            if($this->upload->do_upload('foto'))
            {
                $data = $this->perawatan_model->update_perawatan($id_perawatan, $data);

                if($data['status'])
                {
                    redirect('perawatan/kelola_perawatan', $data['message']);
                }
                else {
                    redirect('perawatan/form_update/'.$id_perawatan);
                }
            }
        }
        else
        {
            $data = array(
                'nama_perawatan'    => $nama_perawatan,
                'deskripsi'         => $deskripsi,
                'jenis_perawatan'   => $jenis_perawatan,
                'durasi'            => $durasi,
                'harga'             => $harga,
                'promo'             => $promo
            );

            $data = $this->perawatan_model->update_perawatan($id_perawatan, $data);

            if($data['status'])
            {
                redirect('perawatan/kelola_perawatan', $data['message']);
            }
            else {
                redirect('perawatan/form_update/'.$id_perawatan);
            }
        }
    }

}