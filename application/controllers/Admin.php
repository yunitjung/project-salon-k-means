<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('pelanggan_model');
    }

    function index()
    {
        $template['title'] = 'Dashboard';
        $template['content'] = $this->load->view('admin/index.php', '', TRUE);
        $this->load->view('admin/template.php', $template);
    }

    function form_update($id)
    {
        $data = array();
        $data['pelanggan'] =  $this->pelanggan_model->view_user_by_id($id);
        $user = $this->session->userdata('roles');
        if($user == '1')
        {
            $data['title'] = 'Kelola Profil';
            $template['content'] = $this->load->view('frontend/kelola_profil', $data, TRUE);
            $this->load->view('admin/template', $template);
        }
    }
}