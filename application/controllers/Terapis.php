<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terapis extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('terapis_model');
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Terapis';
        $data['terapis'] = $this->terapis_model->view_terapis();
        $data['roles'] = $this->session->userdata('roles');
        if($data['roles'] == '1')
        {
            $template['content'] = $this->load->view('admin/jadwal_terapis', $data, TRUE);
            $this->load->view('admin/template', $template);
        }
        else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function delete_terapis($id)
    {
        $this->terapis_model->delete_terapis($id);
        redirect('terapis');
    }

    public function insert_terapis()
    {
        $data['nama_terapis'] = $this->input->post('nama_terapis');
        $data = $this->terapis_model->insert_terapis($data);
        if($data)
        {
            redirect('terapis');
        }
        else
        {
            redirect('terapis');
        }
    }

    public function form_update($id)
    {
        $data = array();
        $data['title'] = 'Update Terapis';
        $data['terapis'] = $this->terapis_model->view_terapis_by_id($id);
        $template['content'] = $this->load->view('admin/update_jadwal_terapis',$data, TRUE);
        $this->load->view('admin/template', $template);
    }

    public function update_terapis()
    {
        $id = $this->input->post('id_terapis');
        $form_data['nama_terapis'] = $this->input->post('nama_terapis');
        $form_data['jam_mulai'] = $this->input->post('jam_mulai');
        $form_data['jam_selesai'] = $this->input->post('jam_selesai');
        $data = $this->terapis_model->update_terapis($id, $form_data);

        if($data['status'])
        {
            redirect('terapis');
        }
        else
        {
            redirect('terapis/form_update/'.$id);
        }


    }


}