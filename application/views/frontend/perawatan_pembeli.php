<main class="main">
            <!-- Breadcrumb -->
            <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Perawatan</a>
                </li>
                <li class="breadcrumb-item active">Perawatan</li>
            </ol>

            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <div class="h5 text-dark"><strong>Perawatan</strong></div>
                                        </div>
                                    </div>
                                    <!-- end clearfix -->
                                    <!-- for products -->
                                    <div class="row">
                                    <?php
                                           $i= 0;
                                            foreach ($perawatan as $d) {
                                                $i++;
                                        ?>
                                        <div class="col-md-3">
                                            <div class="card card-property-single">
                                                <img class="card-img-top" src="<?php print $d['foto']; ?>" alt="Card image cap">
                                                <div class="card-body">
                                                    <div class="address text-theme"> Rp <?php print $d['harga'] ?></div>
                                                    <hr>
                                                    <div class="rent-details">
                                                        <div class="clearfix">
                                                            <div class="float-left text-dark">
                                                                <div class="h5"><strong><?php print $d['nama_perawatan']?></strong></div>
                                                                <small><?php print $d['deskripsi']?></small>
                                                            </div>
                                                            <div class="float-right">
                                                            <?php if($this->session->userdata('username') == ''){?>
                                                                <button class="btn btn-danger btn-sm btn_pesan_pembeli"  data-id= "<?= $d['id_perawatan'] ?>" data-toggle="modal"
                                                                data-target='#daftar_login'>Pesan</button>
                                                            <?php }
                                                                 else { ?>
                                                                 <a href="<?='pemesanan/pemesanan_pelanggan_side/'.$this->session->userdata('username').'/'.$d['id_perawatan']?>" class="btn btn-danger btn-sm">Pesan</a>
                                                                 <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end rent-details -->

                                                </div>
                                                <!-- end card-body -->

                                            </div>
                                            <!-- end card -->
                                        </div>
                                            <?php } ?>
                                        <!-- end inside col -->
                                        <!-- end inside col -->

                                    </div>
                                    <!-- end inside row  -->
                                </div>
                                <!-- end card-body -->
                            </div>
                            <!-- end card -->
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->

                </div>
                <!-- end animated fadeIn -->
                <!-- end container-fluid -->

            </main>


        <div class="modal fade bs-live-example-modal" id="daftar_login" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title"></h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action = "keranjang" method = "post">
                    <div class="modal-body">
                        <div class="form-group">
                        <center>Mau bergabung jadi member?</center>
                        </div>
                    </div>
                    <input type="hidden" name="id_perawatan" id="id_perawatan" value="">
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-secondary">Tidak, saya mau belanja saja</button>
                        <a href = "signup" class="btn btn-danger">Ya </a>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->

        </div>




    </div>

