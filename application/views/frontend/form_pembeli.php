
        <main class = "main">
            <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Form Data Pembeli</a>
                </li>
            </ol>
            <div class="container-fluid" id = "">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <div class="h5 text-dark">Form Data Pembeli</div>
                                        </div>
                                    </div>
                                    <form id= "create_tmp_pelanggan" action="<?= base_url('pelanggan/create_tmp_pelanggan')?>" method="post">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                <input type="text" class = "form-control" name="name" id = "name" placeholder="Masukkan Nama">
                                            </div>
                                            <div class="form-group">
                                                <label for="no_hp">No Hp</label>
                                                <input type="text" name="no_hp" id="no_hp" class="form-control" placeholder="Masukkan no hp" length="13">
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="email" name="email" id="email" class="form-control" placeholder="Masukkan email">
                                            </div>
                                            <input type="hidden" name="id_perawatan" id="id_perawatan" value =<?= $id_perawatan ?>>
                                            <div class="form-group form-actions">
                                                <button type="submit" class="btn  btn-theme login-btn ">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

