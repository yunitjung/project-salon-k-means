<main class = "main">
            <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                <li class="breadcrumb-item ">
                    <a href="">Testimoni</a>
                </li>
                <li class="breadcrumb-item active">Beri Testimoni</li>
            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <div class="h5 text-dark">Berikan Testimoni Kamu Disini!</div>
                                        </div>
                                    </div>
                                    <form action="<?= base_url('testimoni/insert_testimoni') ?>" method = "post">
                                        <input type="hidden" name="username" value = <?= $user->username ?>>
                                        <input type="hidden" name="id_pemesanan" value = <?= $id_pemesanan ?>>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-sm">Nama</div>
                                                <div class="col-sm-1">: </div>
                                                <div class="col-sm"><?= $user->name ?></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm">No Invoice</div>
                                                <div class="col-sm-1">: </div>
                                                <div class="col-sm"><?= $id_pemesanan ?></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm">Kritik</div>
                                                <div class="col-sm-1">: </div>
                                                <div class="col-sm">
                                                    <div class="form-group row">
                                                        <textarea id="kritik" name="kritik" rows="9" class="form-control" placeholder="Kritik.."></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm">Saran</div>
                                                <div class="col-sm-1">: </div>
                                                <div class="col-sm">
                                                    <div class="form-group row">
                                                        <textarea id="saran" name="saran" rows="9" class="form-control" placeholder="Saran.."></textarea>
                                                        <button type="submit" class="btn btn-outline-danger">Kirim</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>