
        <main class = "main">
            <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                <li class="breadcrumb-item ">
                    <a href="">Perawatan</a>
                </li>
                <li class="breadcrumb-item active">Perawatan</li>
            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="card">
                                <div class="card-body" id = "perawatan_pelanggan">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <div class="h5 text-dark">Perawatan</div>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table color-table danger-table">
                                            <th>No ID</th>
                                            <th>Nama Perawatan</th>
                                            <th> Deskripsi </th>
                                            <th> Foto </th>
                                            <th> Jenis Perawatan </th>
                                            <th> Durasi </th>
                                            <th> Harga </th>
                                            <th> Promo </th>
                                            <th> Aksi </th>
                                            <?php
                                            $i = 0;
                                            foreach ($perawatan as $d) {
                                                    $i++;
                                            ?>
                                            <tr>
                                                <td><?php print $i; ?></td>
                                                <td><?php print $d['nama_perawatan'];?></td>
                                                <td> <?php print $d['deskripsi'];?> </td>
                                                <td>
                                                    <img src= "<?= base_url($d['foto']) ?>" width= "50px">
                                                </td>
                                                <td><?php print $d['jenis_perawatan'];?> </td>
                                                <td><?php print $d['durasi'];?> </td>
                                                <td>Rp <?php print $d['harga'];?></td>
                                                <td> <?php print $d['promo'];?> % </td>
                                                <td>
                                                <!-- <button type="button" class="btn btn-sm btn-outline-danger" name = "tambah_pesanan_pelanggan" data-id = "<?php print $d['id_perawatan']?>" data-user = "">Pesan</button> -->
                                                <a href="pemesanan/pemesanan_pelanggan_side/<?= $this->session->userdata('username') ?>/<?= $d['id_perawatan'] ?>" class="btn btn-sm btn-outline-danger">Pesan</a>
                                                </td>
                                            </tr>
                                                <?php }  ?>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>


