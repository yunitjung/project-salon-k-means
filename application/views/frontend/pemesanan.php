
        <main class = "main">
            <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                <li class="breadcrumb-item ">
                    <a href="">Pemesanan</a>
                </li>
                <li class="breadcrumb-item active">Lihat Pemesanan</li>
            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <div class="h5 text-dark">Pemesanan</div>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table color-table danger-table">
                                            <th>No</th>
                                            <th>Tanggal</th>
                                            <th> Durasi </th>
                                            <th> Total Bayar </th>
                                            <th> Status dan Aksi </th>

                                            <?php
                                                $i = 1;
                                                if(!empty($pemesanan)){

                                                    foreach($pemesanan as $d){
                                                ?>
                                                <tr>
                                                    <td><?php echo $i;?></td>
                                                    <td><?php echo $d['tanggal']?></td>
                                                    <td><?php echo $d['total_durasi']?></td>
                                                    <td><?php echo $d['total_bayar']?> </td>
                                                    <td>


                                                        <a href="pemesanan/invoice/<?= $d['id_pemesanan'] ?>" class ="btn btn-sm btn-info" >
                                                            Detail
                                                        </a>
                                                        <a href="<?= base_url('testimoni/tambah_testimoni/'.$this->session->userdata('username').'/'.$d['id_pemesanan'])?>" class="btn btn-sm btn-success" <?php if($d['status_testi']) echo 'hidden' ?>>Beri Testimoni</a>
                                                    </td>
                                                    </tr>
                                                    <?php
                                                    $i++; }
                                                }
                                                else { ?>
                                                    <td colspan = "7">Data kosong</td>
                                                    <?php
                                                }?>
                                                </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    <!-- modal form untuk input pemesanan baru -->
    <div class="modal fade bs-live-example-modal" id="tambahpemesanan" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Tambah Pemesanan</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action = "pemesanan/insert_pemesanan" method = "post">
                    <div class="modal-body">
                        <div class="form-group">
                        <select type="button"  name = "username" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="dropdown-menu">
                                <option class="dropdown-item" value="0">Pilih Nama Pelanggan</option>
                            <?php
                            foreach($user as $d){ ?>
                                <option class="dropdown-item" value="<?php echo $d['username']?>"><?php echo $d['name']?></option>
                            <?php
                            }?>
                            </div>
                        </select>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Tanggal :</label>
                            <input type="date" class="form-control" id="tanggal" name="tanggal" maxlength=50>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary">Reset</button>
                        <button type="submit" class="btn btn-danger">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->

        </div>




    </div>
