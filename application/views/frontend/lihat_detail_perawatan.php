<main class = "main">
            <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                <li class="breadcrumb-item ">
                    <a href="">Perawatan</a>
                </li>
                <li class="breadcrumb-item active"></li>
            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body" id = "lihat_detail_perawatan">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <div class="h5 text-dark"><strong>Perawatan</strong></div>
                                        </div>
                                    </div>
                                    <!-- end clearfix -->
                                    <!-- for products -->
                                    <?php
                                        $id_pemesanan = $_GET['id_pemesanan'];
                                        $id_perawatan = $_GET['id'];
                                        $data = $controller->view_perawatan_edit($id_perawatan);
                                        foreach($data as $d){
                                    ?>
                                    <div class="alert alert-success alert-dismissible fade show" id = "alert_pemesanan_pelanggan" role="alert" style = "opacity: 0; display: none">
                                        Data Berhasil Dimasukkan
                                        <a href="kelola_pemesanan.php?page=pemesanan_controller&id=<?php echo $id_pemesanan?>" class="btn btn-success">Cek Pemesanan</a>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="card card-property-single">
                                                <img class="card-img-top" src="<?php print $d['foto'] ?>" alt="<?php print $d['nama_perawatan'] ?>">
                                                <div class="card-body">
                                                    <div class="address text-theme"> <?php print $d['nama_perawatan'] ?></div>
                                                    <hr>
                                                    <div class="rent-details">
                                                        <div class="clearfix">
                                                            <div class="float-left text-dark">
                                                                <div class="h5"><strong>Rp <?php print $d['harga']?></strong></div>
                                                                <small><?php print $d['jenis_perawatan']?></small>
                                                            </div>
                                                            <div class="float-right">
                                                                <button class="btn btn-danger btn-sm" id = "tambah_pesanan_detail" data-id_pemesanan="<?php echo $id_pemesanan?>" data-id_perawatan="<?php echo $id_perawatan?>">Pesan</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end rent-details -->

                                                </div>
                                                <!-- end card-body -->

                                            </div>
                                            <!-- end card -->
                                        </div>
                                        <!-- end inside col -->
                                        <!-- end inside col -->
                                        <div class="col-md-6">
                                            <p><?php print $d['deskripsi']?></p>
                                            <h5>Durasi : <?php print $d['durasi'] ?></h5>
                                        </div>
                                    </div>
                                        <?php } ?>
                                    <!-- end inside row  -->
                                </div>
                                <!-- end card-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>