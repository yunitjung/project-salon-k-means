<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title><?php echo $title ?></title>

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url()?>/assets/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('assets/img/logo.jpg')?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets/img/logo.jpg')?>">
    <link rel="manifest" href="<?php echo base_url()?>/assets/img/favicon/manifest.json">
    <link rel="mask-icon" href="<?php echo base_url()?>/assets/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">

    <!-- fonts -->
    <link rel="stylesheet" href="<?php echo base_url()?>/assets/fonts/md-fonts/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>/assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="<?php echo base_url()?>/assets/libs/animate.css/animate.min.css">

     <!-- jquery-loading -->
     <link rel="stylesheet" href="<?php echo base_url()?>/assets/libs/jquery-loading/dist/jquery.loading.min.css">
    <!-- octadmin main style -->
    <link id="pageStyle" rel="stylesheet" href="<?php echo base_url()?>/assets/css/style-google-plus.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script>var base_url = '<?php echo base_url() ?>';</script>
</head>
<body class = "app sidebar-fixed aside-menu-off-canvas aside-menu-hidden header-fixed">
    <header class="app-header navbar">
        <div class="hamburger hamburger--arrowalt-r navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>
        <!-- end hamburger -->
        <a class="navbar-brand" href="<?= base_url('perawatan') ?>">
            <strong>STAR</strong>
        </a>

        <div class="hamburger hamburger--arrowalt-r navbar-toggler sidebar-toggler d-md-down-none mr-auto">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>
        <!-- end hamburger -->

        <!-- <div class="navbar-search">
            <button type="submit" class="navbar-search-btn">
                <i class="mdi mdi-magnify"></i>
            </button>
            <input type="text" class="navbar-search-input" placeholder="Find User a user, team, meeting ..">
        </div> -->
        <!-- end navbar-search -->

        <ul class="nav navbar-nav ">

            <li class="nav-item dropdown">
                <a class="btn btn-round btn-theme btn-sm" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">

                    <span class="">
                        <i class="fa fa-arrow-down"></i><?= $this->session->userdata('name')?>
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right user-menu animated flipInY ">
                    <div class="wrap">
                        <div class="dw-user-box">
                            <div class="u-img">
                                <img src="http://via.placeholder.com/100x100" alt="user" />
                            </div>
                            <div class="u-text">
                                <h5><?= $this->session->userdata('name')?></h5>
                                <a href="<?= base_url('my-profile/'.$this->session->userdata('username'))?>" class="btn btn-round btn-theme btn-sm">View Profile</a>
                            </div>
                        </div>
                        <!-- end dw-user-box -->


                        <div class="divider"></div>

                        <a class="dropdown-item" href="<?= base_url('logout') ?>">
                            <i class="fa fa-lock"></i> Logout</a>
                    </div>
                    <!-- end wrap -->
                </div>
                <!-- end dropdown-menu -->
            </li>
            <!-- end nav-item -->


        </ul>


    </header>
        <!-- end header -->


    <div class="app-body">
        <div class="sidebar" id="sidebar">
            <nav class="sidebar-nav" id="sidebar-nav-scroller">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('perawatan')?>"> <i class="mdi mdi-flower"></i>Perawatan</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="http://localhost/sherly/view/pelanggan/kelola_pemesanan.php?page=pemesanan_controller">  <i class="mdi mdi-atom"></i>Kelola Pemesanan</a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('pemesanan')?>">  <i class="mdi mdi-cart"></i>Lihat Pemesanan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('my-profile/'.$this->session->userdata('username'))?>">  <i class="mdi mdi-ticket-account"></i>Kelola Profil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://localhost/sherly/view/pelanggan/testimoni.php"> <i class="mdi mdi-comment-processing"></i>Testimoni</a>
                    </li>
                </ul>
            </nav>

        </div>


        <?php

        if(isset($content))
        {
            echo $content;
        }

        ?>

    </div>
       <footer class="app-footer">
        <a href="#" class="text-theme">octAdmin</a> &copy; 2018 octaThemes.
    </footer>

    <!-- Bootstrap and necessary plugins -->
    <script src="<?php echo base_url()?>assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo base_url()?>assets/libs/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/libs/PACE/pace.min.js"></script>
    <script src="<?php echo base_url()?>assets/libs/chart.js/dist/Chart.min.js"></script>
    <script src="<?php echo base_url()?>assets/libs/nicescroll/jquery.nicescroll.min.js"></script>

     <!--Flot Chart -->
    <script src="<?php echo base_url()?>assets/libs/charts-flot/jquery.flot.js"></script>
    <script src="<?php echo base_url()?>assets/libs/charts-flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url()?>assets/libs/charts-flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url()?>assets/libs/charts-flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url()?>assets/libs/charts-flot/jquery.flot.crosshair.js"></script>


    <!--morris js -->
    <script src="<?php echo base_url()?>assets/libs/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url()?>assets/libs/charts-morris-chart/morris.min.js"></script>

       <!-- jquery-loading -->
    <script src="<?php echo base_url()?>assets/libs/jquery-loading/dist/jquery.loading.min.js"></script>

    <!-- octadmin Main Script -->
    <script src="<?php echo base_url()?>assets/js/app.js"></script>

    <!-- dashboard-property-examples -->
    <script src="<?php echo base_url()?>assets/js/dashboard-property-examples.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="<?php echo base_url()?>assets/js/admin.js"></script>
    <script src = "<?php echo base_url()?>assets/js/engine.js"></script>
</body>
</html>