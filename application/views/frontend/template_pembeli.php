<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title><?php echo $title ?></title>

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url()?>assets/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('assets/img/logo.jpg')?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets/img/logo.jpg')?>">
    <link rel="manifest" href="<?php echo base_url()?>assets/img/favicon/manifest.json">
    <link rel="mask-icon" href="<?php echo base_url()?>assets/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">

    <!-- fonts -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/fonts/md-fonts/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/libs/animate.css/animate.min.css">

     <!-- jquery-loading -->
     <link rel="stylesheet" href="<?php echo base_url()?>assets/libs/jquery-loading/dist/jquery.loading.min.css">
    <!-- octadmin main style -->
    <link id="pageStyle" rel="stylesheet" href="<?php echo base_url()?>assets/css/style-google-plus.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script>var base_url = '<?php echo base_url() ?>';</script>
</head>
<body class = "app aside-menu-off-canvas aside-menu-hidden header-fixed">

    <header class="app-header navbar">
        <!-- end hamburger -->
        <a class="navbar-brand" href="<?= base_url('perawatan') ?>">
            <strong>STAR</strong>
        </a>

        <!-- end hamburger -->

        <!-- <div class="navbar-search">
            <button type="submit" class="navbar-search-btn">
                <i class="mdi mdi-magnify"></i>
            </button>
            <input type="text" class="navbar-search-input" placeholder="Find User a user, team, meeting ..">
        </div> -->
        <!-- end navbar-search -->

        <nav class="sidebar-nav" id="sidebar-nav-scroller">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('home') ?>">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('perawatan') ?>">Perawatan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Contact</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('login') ?>">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('signup') ?>">Sign Up</a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link"><i class="mdi mdi-cart-outline"></i></a>
                    </li>
                </ul>
            </nav>
    </header>


    <div class="app-body">

        <?php

        if(isset($content))
        {
            echo $content;
        }

        ?>

    </div>
       <footer class="app-footer">
        <a href="#" class="text-theme">octAdmin</a> &copy; 2018 octaThemes.
    </footer>

    <!-- Bootstrap and necessary plugins -->
    <script src="<?php echo base_url()?>assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo base_url()?>assets/libs/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/libs/PACE/pace.min.js"></script>
    <script src="<?php echo base_url()?>assets/libs/chart.js/dist/Chart.min.js"></script>
    <script src="<?php echo base_url()?>assets/libs/nicescroll/jquery.nicescroll.min.js"></script>

     <!--Flot Chart -->
    <script src="<?php echo base_url()?>assets/libs/charts-flot/jquery.flot.js"></script>
    <script src="<?php echo base_url()?>assets/libs/charts-flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url()?>assets/libs/charts-flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url()?>assets/libs/charts-flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url()?>assets/libs/charts-flot/jquery.flot.crosshair.js"></script>


    <!--morris js -->
    <script src="<?php echo base_url()?>assets/libs/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url()?>assets/libs/charts-morris-chart/morris.min.js"></script>

       <!-- jquery-loading -->
    <script src="<?php echo base_url()?>assets/libs/jquery-loading/dist/jquery.loading.min.js"></script>

    <!-- octadmin Main Script -->
    <script src="<?php echo base_url()?>assets/js/app.js"></script>

    <!-- dashboard-property-examples -->
    <script src="<?php echo base_url()?>assets/js/dashboard-property-examples.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="<?php echo base_url()?>assets/js/admin.js"></script>
    <script src = "<?php echo base_url()?>assets/js/engine.js"></script>
</body>
</html>