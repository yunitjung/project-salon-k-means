<main class = "main">
            <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                <li class="breadcrumb-item ">
                    <a href="">Profil</a>
                </li>
                <li class="breadcrumb-item active">Kelola Profil</li>
            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <div class="h5 text-dark">Kelola Profil</div>
                                        </div>
                                    </div>
                                        <div class="card-body">
                                        <form action="<?= base_url('pelanggan/update_pelanggan') ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                                            <div class="form-group row">
                                                <label class="col-md-3 form-control-label">Username</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?= $pelanggan->username ?></p>
                                                </div>
                                                <input type = "hidden" name = "username" value = "<?= $pelanggan->username ?>">
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 form-control-label" for="password-input">Password</label>
                                                <div class="col-md-9">
                                                    <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
                                                    <span class="help-block">Please enter a complex password</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 form-control-label" for="text-input">Nama</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="name" name="name" class="form-control" placeholder="Nama" value = "<?= $pelanggan->name ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 form-control-label" for="email-input">Email Input</label>
                                                <div class="col-md-9">
                                                    <input type="email" id="email" name="email" class="form-control" placeholder="Email" value = "<?= $pelanggan->email ?>" required>
                                                    <span class="help-block">Please enter your email</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 form-control-label" for="text-input">No HP</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="no_hp" name="no_hp" class="form-control" placeholder="No HP" value = "<?= $pelanggan->no_hp ?>" required>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group row">
                                                <label class="col-md-3 form-control-label" for="file-multiple-input">Foto</label>
                                                <div class="col-md-9">
                                                    <input type="file" id="file-multiple-input" name="file-multiple-input" multiple="">
                                                </div>
                                            </div> -->

                                                <button type="submit" class="btn btn-sm btn-primary">
                                                    <i class="fa fa-dot-circle-o"></i> Submit</button>
                                                <button type="reset" class="btn btn-sm btn-danger">
                                                    <i class="fa fa-ban"></i> Reset</button>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>