



        <main class = "main">
            <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Pemesanan</a>
                </li>
                <li class="breadcrumb-item active">Tambah Pemesanan</li>
            </ol>
            <div class="container-fluid" id = "pesan_pelanggan">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <div class="h5 text-dark">Pilih Pesanan</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class = "table-responsive">
                                                <table class="table table-bordered color-table danger-table">
                                                    <tr><th>Id Pemesanan</th><td><?php echo $pemesanan->id_pemesanan ?></td></tr>
                                                    <tr><th>Tanggal</th><td><?php echo $pemesanan->tanggal?></td></tr>
                                                    <tr><th>Nama</th><td><?php echo $pemesanan->name?> </td></tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="alert alert-success" <?php if($pemesanan->status == '1' || $pemesanan->status == '2') echo 'hidden'?>>
                                                <span id="label-alert">Pesananmu telah diterima!</span>
                                                <a href="<?= base_url('perawatan') ?>" class="btn btn-info">Lanjut Belanja</a>
                                                <button class="btn btn-danger" id = "btn_bayar" name = "btn_bayar" data-id="<?= $pemesanan->id_pemesanan ?>">Selesai Belanja</button>
                                                <button type = "button" class="btn btn-info" id ="btn_redeem_voucher" data-toggle = "modal" data-target ='#modal_voucher' disabled>Redeem Voucher</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table color-table danger-table">

                                                <th> Nama Perawatan</th>
                                                <th> Durasi </th>
                                                <th> Harga </th>
                                                <th> Diskon </th>
                                                <th></th>
                                                <tr>
                                                <?php
                                                    if(!empty($detail_trx)){
                                                        $total_harga = 0;
                                                        $total_durasi = 0;
                                                        $diskon = 0;
                                                        $diskon_voucher = 0;
                                                        if($this->session->userdata('besar_diskon') !== '')
                                                            $diskon_voucher = (int)$this->session->userdata('besar_diskon');
                                                ?>
                                                <?php foreach($detail_trx as $dd){
                                                    $total_harga+=$dd['harga'];
                                                    $diskon+=$dd['promo']*$dd['harga']/100;
                                                    $total_harga-=$dd['promo']*$dd['harga']/100;
                                                    $total_durasi+=$dd['durasi'];
                                                     ?>
                                                <tr>

                                                    <td><?php echo $dd['nama_perawatan']?></td>
                                                    <td><?php echo $dd['durasi']?></td>
                                                    <td><?php echo $dd['harga']?></td>
                                                    <td><?php echo $dd['promo'] ?> %</td>
                                                    <?php if($pemesanan->status == '0') {?>
                                                    <td>
                                                        <a href="<?= base_url('pemesanan/delete_detail_trx/'.$pemesanan->id_pemesanan.'/'.$dd['id_detail_trx']) ?>" data-toggle="tooltip" data-original-title="Hapus">
                                                            <i class="fa fa-close text-danger"></i>
                                                        </a>
                                                    </td>
                                                    <?php }
                                                    else {
                                                        ?>
                                                        <td></td>
                                                    <?php }
                                                     ?>
                                                </tr>
                                                <?php } ?>
                                                <form method = "post" name = "form_bayar" id = "form_bayar">
                                                <input type="hidden" name="id_pemesanan" value = <?= $pemesanan->id_pemesanan ?>>
                                                <input type="hidden" name="total_harga" value = <?= $total_harga ?> >
                                                <input type="hidden" name="total_durasi" value = <?= $total_durasi ?> >
                                                    </form>
                                            <?php }
                                                else { ?>
                                                    <td colspan = "6"></td>
                                                    <?php
                                                }
                                                if($pemesanan->status == '1' || $pemesanan->status == '2')


                                                 { ?>
                                                   <tr>
                                                     <td colspan = "2"><strong>Voucher</strong></td>
                                                     <td><?php echo '-(' .floor($diskon_voucher * $total_harga / 100).')' ?></td>
                                                     <td><?php echo $diskon_voucher . ' %' ?></td>
                                                 </tr>
                                                 <tr>
                                                     <td colspan = "2"><strong>Diskon</strong></td>
                                                     <td><?php echo '-(' .$diskon.')' ?></td>
                                                     <td></td>
                                                 </tr>
                                                <tr>
                                                    <td>
                                                        <strong>Total</strong>
                                                    </td>
                                                    <td>
                                                        <strong><?php echo $pemesanan->total_durasi?></strong>
                                                    </td>
                                                    <td>
                                                        <strong><?php echo $pemesanan->total_bayar ?></strong>
                                                    </td>
                                                    <td colspan = "2"></td>
                                                </tr>
                                                <?php } ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>


        <div class="modal fade" id="modal_voucher" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Redeem Voucher</h6>
                    <button type="button" id = "btn_close_voucher" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form method = "post" id = "form_voucher" name = "form_voucher" action = "#">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="kode_voucher" class="col-form-label">Voucher :</label>
                        <input type="text" class="form-control" id="kode_voucher" name="kode_voucher" maxlength=50>
                        <input type="hidden" name="username" value = "<?= $this->session->userdata('username')?>">
                        <input type="hidden" name="id_pemesanan" value = "<?= $pemesanan->id_pemesanan ?>">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-secondary">Reset</button>
                    <a class="btn btn-danger" id = "btn_voucher">Redeem</a>
                </div>
            </form>
            </div>
            <!-- /.modal-content -->
        </div>
        </div>