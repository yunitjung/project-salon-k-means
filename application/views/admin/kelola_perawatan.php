
        <main class = "main">
            <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                <li class="breadcrumb-item ">
                    <a href="">Perawatan</a>
                </li>
                <li class="breadcrumb-item active">Kelola Perawatan</li>
            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <div class="h5 text-dark">Kelola Perawatan</div>
                                            <button type="submit" class="btn btn-outline-danger" data-toggle="modal" data-target="#tambahperawatan">Tambah Perawatan</button>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table color-table danger-table">
                                                <th>No ID</th>
                                                <th>Nama Perawatan</th>
                                                <th> Deskripsi </th>
                                                <th> Foto </th>
                                                <th> Jenis Perawatan </th>
                                                <th> Durasi </th>
                                                <th> Harga </th>
                                                <th> Promo </th>
                                                <th> Aksi </th>
                                                <?php
                                                // $data = $controller->view_perawatan();
                                                foreach ($perawatan as $d) { ?>
                                                <tr>
                                                    <td><?php print $d['id_perawatan'] ?></td>
                                                    <td><?php print $d['nama_perawatan'] ?></td>
                                                    <td><?php print $d['deskripsi'] ?></td>
                                                    <td>
                                                        <img src= "<?php  echo base_url().$d['foto'] ?>" width= "100px">
                                                    </td>
                                                    <td><?php print $d['jenis_perawatan'] ?> </td>
                                                    <td><?php print $d['durasi']?> Menit </td>
                                                    <td><?php print $d['harga']?></td>
                                                    <td><?php print $d['promo']?> % </td>
                                                    <td>
                                                        <a href="form_update/<?=$d['id_perawatan'] ?>" data-toggle="tooltip" data-original-title="Edit">
                                                            <i class="fa fa-pencil text-inverse m-r-10"></i>
                                                        </a>
                                                        <a href="delete_perawatan/<?= $d['id_perawatan']?>" data-toggle="tooltip" data-original-title="Hapus">
                                                            <i class="fa fa-close text-danger"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <?php }?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <!-- modal form untuk input perawatan baru -->
        <div class="modal fade bs-live-example-modal" id="tambahperawatan" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Tambah Perawatan</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <!-- <form action = "insert_perawatan" method = "post" enctype="multipart/form-data"> -->
                <?php echo form_open_multipart('perawatan/insert_perawatan'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Nama Perawatan:</label>
                            <input type="text" class="form-control" id="nama_perawatan" name="nama_perawatan" maxlength=50>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Deskripsi:</label>
                            <textarea class="form-control" id="deskripsi" name="deskripsi" maxlength=100></textarea>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Foto:</label>
                            <input type="file" class="form-control" id="foto" name="foto">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Jenis Perawatan:</label>
                            <input type="text" class="form-control" id="jenis_perawatan" name="jenis_perawatan" maxlength=50>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Durasi:</label>
                            <input type="number" class="form-control" id="durasi" name="durasi" maxlength=11>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Harga:</label>
                            <input type="number" class="form-control" id="harga_perawatan" name="harga_perawatan" maxlength=11>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Promo:</label>
                            <input type="number" class="form-control" id="promo" name="promo" maxlength=11>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary">Reset</button>
                        <button type="submit" class="btn btn-danger">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->

        </div>
        <!-- /.modal-dialog -->
