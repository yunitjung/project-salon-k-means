
        <main class = "main">
            <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                <li class="breadcrumb-item ">
                    <a href="">Terapis</a>
                </li>
                <li class="breadcrumb-item active">Jadwal Terapis</li>
            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <div class="h5 text-dark">Jadwal Terapis</div>
                                            <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#tambahterapis">Tambah Terapis</button>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table color-table danger-table">
                                                <th>No</th>
                                                <th> Nama Terapis</th>
                                                <th> Jam Mulai Kerja </th>
                                                <th> Jam Selesai Kerja </th>
                                                <th> Aksi </th>
                                                <?php

                                                    foreach ($terapis as $d) {
                                                        $i = 1;

                                                ?>
                                                <tr>
                                                    <td><?php print $d['id_terapis']?></td>
                                                    <td><?php print $d['nama_terapis']?> </td>
                                                    <td><?php print $d['jam_mulai']?></td>
                                                    <td><?php print $d['jam_selesai']?></td>
                                                    <td>
                                                        <a href="terapis/form_update/<?php echo $d['id_terapis']?>" data-toggle="tooltip" data-original-title="Edit">
                                                            <i class="fa fa-pencil text-inverse m-r-10"></i>
                                                        </a>
                                                        <a href="terapis/delete_terapis/<?= $d['id_terapis']?>" data-toggle="tooltip" data-original-title="Hapus">
                                                            <i class="fa fa-close text-danger"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <?php $i++; } ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- modal form untuk input perawatan baru -->
        <div class="modal fade bs-live-example-modal" id="tambahterapis" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Tambah Terapis</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action = "terapis/insert_terapis" method = "post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nama_terapis" class="col-form-label">Nama :</label>
                            <input type="text" class="form-control" id="nama_terapis" name="nama_terapis" maxlength=50>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary">Reset</button>
                        <button type="submit" class="btn btn-danger">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        </div>
        <!-- /.modal-dialog -->

