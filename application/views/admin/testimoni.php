
        <main class = "main">
            <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                <li class="breadcrumb-item ">
                    <a href="">Testimoni</a>
                </li>
                <li class="breadcrumb-item active">Kelola Testimoni</li>
            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <div class="h5 text-dark">Testimoni</div>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table color-table danger-table">
                                                <th>No</th>
                                                <th> Nama </th>
                                                <th> Invoice </th>
                                                <th> Kritik </th>
                                                <th> Saran </th>
                                                <th> Aksi </th>
                                                <?php
                                                $i = 1;
                                                foreach($testimoni as $t){ ?>
                                                <tr>
                                                    <td><?= $i ?></td>
                                                    <td><?= $t['name'] ?></td>
                                                    <td><a href="<?= base_url('pemesanan/invoice/').$t['id_pemesanan']?>" class="btn btn-sm btn-info"> Transaksi - <?= $t['id_pemesanan']?></a></td>
                                                    <td><?= $t['kritik']?> </td>
                                                    <td><?= $t['saran']?> </td>
                                                    <td>
                                                        <a href="<?= base_url('testimoni/delete_testimoni/'.$t['id_testimoni'])?>" data-toggle="tooltip" data-original-title="Hapus">
                                                            <i class="fa fa-trash-o text-danger"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <?php
                                                $i++;
                                                } ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
