
        <main class="main">
            <!-- Breadcrumb -->
            <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                <li class="breadcrumb-item ">
                    <a href="">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">Dashboards</a>
                </li>
                <li class="breadcrumb-item active">Dashboard Property</li>
            </ol>

            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <div class="h5 text-dark"><strong>Dashboard</strong></div>
                                        </div>
                                    </div>
                                    <!-- end clearfix -->
                                    <!-- for products -->
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="card card-property-single">
                                                <img class="card-img-top" src="http://via.placeholder.com/350x250" alt="Card image cap">
                                                <div class="card-body">
                                                    <div class="address text-theme"> 180 Jasper Lane #108 <br/> pittsburgh PA 82110</div>
                                                    <hr>
                                                    <div class="rent-details">
                                                        <div class="clearfix">
                                                            <div class="float-left text-dark">
                                                                <div class="h5"><strong>$ 1,000 per month</strong></div>
                                                                <small>2 YEAR LEASE REQUIRED</small>
                                                            </div>
                                                            <div class="float-right">
                                                                <button class="btn btn-danger btn-sm">Book</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end rent-details -->

                                                </div>
                                                <!-- end card-body -->

                                            </div>
                                            <!-- end card -->
                                        </div>
                                        <!-- end inside col -->
                                        <!-- end inside col -->

                                    </div>
                                    <!-- end inside row  -->
                                </div>
                                <!-- end card-body -->
                            </div>
                            <!-- end card -->
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->

                </div>
                <!-- end animated fadeIn -->
                <!-- end container-fluid -->

            </main>