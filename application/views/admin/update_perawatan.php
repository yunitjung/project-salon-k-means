
        <main class = "main">
            <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                <li class="breadcrumb-item ">
                    <a href="">Perawatan</a>
                </li>
                <li class="breadcrumb-item active">Kelola Perawatan</li>
            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <div class="h5 text-dark">Update Perawatan</div>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table color-table danger-table">
                                                <th>Nama Perawatan</th>
                                                <th> Deskripsi </th>
                                                <th> Foto </th>
                                                <th> Jenis Perawatan </th>
                                                <th> Durasi </th>
                                                <th> Harga </th>
                                                <th> Promo </th>
                                                <th> Aksi </th>
                                                <tr>
                                                <!-- <form id= "update_perawatan" action="update_perawatan" method="post" enctype="multipart/form-data"> -->
                                                <?php echo form_open_multipart('perawatan/update_perawatan'); ?>
                                                <?php foreach($perawatan as $d){ ?>
                                                    <input type = "hidden" name="id_perawatan" id="id_perawatan" value=<?php print $d['id_perawatan']?>>
                                                    <td>
                                                    <input type="text" class = "form-control" value ="<?php print $d['nama_perawatan']?>" name="nama_perawatan" id = "nama_perawatan" required >
                                                    </td>
                                                    <td>
                                                    <input type="text" class = "form-control" value ="<?php print $d['deskripsi']?>" name="deskripsi" id = "deskripsi" required >
                                                    </td>
                                                    <td>
                                                    <input type="file" class="form-control" id="foto" name="foto" value="<?php print $d['foto']?>" >
                                                    </td>
                                                    <td>
                                                    <input type="text" class = "form-control" value ="<?php print $d['jenis_perawatan']?>" name="jenis_perawatan" id = "jenis_perawatan" required>
                                                    </td>
                                                    <td>
                                                    <input type="number" class="form-control" id="durasi" name="durasi" maxlength=11 value =<?php print $d['durasi']?> required>
                                                    </td>
                                                    <td>
                                                    <input type="number" class="form-control" id="harga" name="harga" maxlength=11 value =<?php print $d['harga']?> required>
                                                    </td>
                                                    <td>
                                                    <input type="number" class="form-control" id="promo" name="promo" maxlength=11 value=<?php print $d['promo'] ?>>
                                                    </td>
                                                    <td>
                                                        <button type ="submit" id="button_submit_pelanggan" class = "btn btn-outline-danger" name="button_submit_pelanggan"> Submit</button>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </form>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
