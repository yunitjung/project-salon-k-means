
        <main class = "main">
            <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                <li class="breadcrumb-item ">
                    <a href="">Pelanggan</a>
                </li>
                <li class="breadcrumb-item active">Data Pelanggan</li>
            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <div class="h5 text-dark">Data Pelanggan</div>
                                            <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#tambahpelanggan">Tambah Pelanggan</button>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table color-table danger-table">
                                                <th>No</th>
                                                <th> Nama </th>
                                                <th> Username </th>
                                                <th> Email </th>
                                                <th> Nomor Handphone </th>
                                                <th> Aksi </th>
                                                <?php
                                                    $i=0;
                                                    foreach ($pelanggan as $d) {
                                                        $i++;
                                                ?>
                                                <tr>
                                                    <td><?php print $i ?></td>
                                                    <td><?php print $d['name']?> </td>
                                                    <td> <?php print $d['username']?> </td>
                                                    <td> <?php print $d['email']?></td>
                                                    <td> <?php print $d['no_hp']?> </td>
                                                    <td>
                                                        <a href="pelanggan/form_update/<?php echo $d['username'] ?>" data-toggle="tooltip" data-original-title="Edit" data-target="#editpelanggan">
                                                            <i class="fa fa-pencil text-inverse m-r-10"></i>
                                                        </a>
                                                        <a href="pelanggan/delete_user/<?= $d['username'] ?>" id = "delete-pelanggan" data-toggle="tooltip" data-original-title="Hapus">
                                                            <i class="fa fa-close text-danger"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                    <?php } ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- modal form untuk input perawatan baru -->
        <div class="modal fade bs-live-example-modal" id="tambahpelanggan" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Tambah Pelanggan</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action = "pelanggan/insert_user" method = "post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Nama :</label>
                            <input type="text" class="form-control" id="name" name="name" maxlength=50>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Username :</label>
                            <textarea class="form-control" id="username" name="username" maxlength=100></textarea>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Password :</label>
                            <input type="password" class="form-control" id="password" name="password" maxlength=8>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Email :</label>
                            <input type="text" class="form-control" id="email" name="email" maxlength=50>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">No Hp:</label>
                            <input type="number" class="form-control" id="no_hp" name="no_hp" maxlength=11>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary">Reset</button>
                        <button type="submit" class="btn btn-danger">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->

        </div>



