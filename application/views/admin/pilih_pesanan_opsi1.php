
        <main class = "main">
            <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Pemesanan</a>
                </li>
                <li class="breadcrumb-item active">Tambah Pemesanan</li>
            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <div class="h5 text-dark">Pilih Pesanan</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class = "table-responsive">
                                                <table class="table table-bordered color-table danger-table">
                                                    <tr><th>Id Pemesanan</th><td><?php echo $pemesanan->id_pemesanan ?></td></tr>
                                                    <tr><th>Tanggal</th><td><?php echo $pemesanan->tanggal?></td></tr>
                                                    <tr><th>Nama</th><td><?php echo $pemesanan->name?> </td></tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="alert alert-danger" <?php echo ($status == 1 || $status == '') ? 'hidden' : '' ?>>
                                                Durasi terlalu lama ! Pilih perawatan/terapis lain
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table color-table danger-table">

                                                <th> Nama Perawatan</th>
                                                <th> Nama Terapis </th>
                                                <th> Durasi </th>
                                                <th> Harga </th>
                                                <th> Diskon </th>
                                                <?php if($pemesanan->status != '2') {?>
                                                <th> Aksi </th>
                                                <?php } ?>
                                                <tr>
                                                <?php if($pemesanan->status == '0') {?>
                                                <form id= "insert_detail_trx" action="<?= base_url('pemesanan/insert_detail_trx') ?>" method="post">
                                                    <input type="hidden" name="id_pemesanan" value="<?php echo $pemesanan->id_pemesanan?>">
                                                    <td>
                                                        <select type="button"  name = "id_perawatan" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <div class="dropdown-menu">
                                                                <option class="dropdown-item" value="0">Pilih Jenis Perawatan</option>
                                                            <?php
                                                            foreach($perawatan as $dp){ ?>
                                                                <option class="dropdown-item" value="<?php echo $dp['id_perawatan']?>"><?php echo $dp['nama_perawatan']?></option>
                                                            <?php
                                                            }?>
                                                            </div>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select type="button"  name = "id_terapis" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <div class="dropdown-menu">
                                                                <option class="dropdown-item" value="0">Pilih Terapis</option>
                                                            <?php
                                                            foreach($terapis as $dt){ ?>
                                                                <option class="dropdown-item" value="<?php echo $dt['id_terapis']?>"><?php echo $dt['nama_terapis']?></option>
                                                            <?php
                                                            }?>
                                                            </div>
                                                        </select>
                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <button type ="submit" id="button_submit_pelanggan" class = "btn btn-outline-danger" name="button_submit_pelanggan"> Submit</button>
                                                    </td>
                                                </tr>
                                                </form>
                                                <?php
                                                }
                                                    if(!empty($detail_trx)){
                                                        $total_harga = 0;
                                                        $total_durasi = 0;
                                                        $diskon = 0;
                                                ?>
                                                <?php foreach($detail_trx as $dd){
                                                    $total_harga+=$dd['harga'];
                                                    $diskon+=$dd['promo']*$dd['harga']/100;
                                                    $total_harga-=($dd['promo']*$dd['harga']/100);
                                                    $total_durasi+=$dd['durasi'];
                                                     ?>
                                                <tr>

                                                    <td><?php echo $dd['nama_perawatan']?></td>
                                                    <td><?php
                                                    if($dd['nama_terapis'])
                                                        echo $dd['nama_terapis'];
                                                    else { ?>
                                                        <select type="button"  name = "id_terapis" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <div class="dropdown-menu">
                                                                <option class="dropdown-item" value="0">Pilih Terapis</option>
                                                            <?php
                                                            foreach($terapis as $dt){ ?>
                                                                <option class="dropdown-item" value="<?php echo $dt['id_terapis']?>"><?php echo $dt['nama_terapis']?></option>
                                                            <?php
                                                            }?>
                                                            </div>
                                                        </select>
                                                    <?php }
                                                        ?></td>

                                                    <td><?php echo $dd['durasi']?></td>
                                                    <td><?php echo $dd['harga']?></td>
                                                    <td><?php echo $dd['promo']?> %</td>
                                                    <?php if($pemesanan->status == '1') {?>
                                                    <td>
                                                        <?php if(empty($dd['nama_terapis']))
                                                        {?>
                                                            <button type = "submit" id="update_terapis" class = "btn btn-outline-danger" data-id = '<?= $pemesanan->id_pemesanan ?>' data-id_detail = "<?= $dd['id_detail_trx'] ?>">Submit</button>
                                                         <?php
                                                        } ?>
                                                        <a href="<?= base_url('pemesanan/delete_detail_trx/'.$pemesanan->id_pemesanan.'/'.$dd['id_detail_trx']) ?>" data-toggle="tooltip" data-original-title="Hapus">
                                                            <i class="fa fa-close text-danger"></i>
                                                        </a>

                                                    </td>
                                                    <?php }
                                                     ?>
                                                </tr>
                                                <?php } ?>

                                            <?php }
                                                else { ?>
                                                    <td colspan = "7"></td>
                                                    <?php
                                                }?>

                                                <?php if($pemesanan->status !== '2') {?>
                                                <tr>
                                                    <td colspan=7 class="text-right">
                                                    <form method = "post" action = "<?= base_url('pemesanan/update_status_for_admin/'.$pemesanan->id_pemesanan)?>">
                                                        <input type="hidden" name="id_pemesanan" value = <?php echo (isset($pemesanan)) ? $pemesanan->id_pemesanan : "" ?>>
                                                        <input type="hidden" name="total_bayar" value = <?php echo (isset($total_harga)) ? $total_harga : "" ?> >
                                                        <input type="hidden" name="total_durasi" value = <?php echo (isset($total_durasi)) ? $total_durasi : "" ?> >
                                                        <button type = "submit" id = "btn_update_status" class = "btn btn-outline-danger" <?php if(!isset($pemesanan)) echo 'disabled' ?>>Finish</button>
                                                    </td>
                                                    </form>
                                                </tr>
                                                <?php }
                                                else { ?>

                                                <tr>
                                                    <td colspan = 3>
                                                        <strong>Diskon</strong>
                                                    </td>
                                                    <td><?php echo '-('.$diskon.')' ?></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td colspan=2>
                                                        <strong>Total</strong>
                                                    </td>
                                                    <td>
                                                        <strong><?php echo $pemesanan->total_durasi?></strong>
                                                    </td>
                                                    <td>
                                                        <strong><?php echo $pemesanan->total_bayar ?></strong>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <?php } ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

