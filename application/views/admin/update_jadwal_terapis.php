
        <main class = "main">
            <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                <li class="breadcrumb-item ">
                    <a href="">Terapis</a>
                </li>
                <li class="breadcrumb-item active">Update Terapis</li>
            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <div class="h5 text-dark">Jadwal Terapis</div>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table color-table danger-table">
                                                <th> Nama Terapis</th>
                                                <th> Jam Mulai Kerja </th>
                                                <th> Jam Selesai Kerja </th>
                                                <th> Aksi </th>
                                                <?php

                                                    $i = 1;
                                                ?>
                                                <tr>
                                                <form id= "update_terapis" action="<?=base_url('terapis/update_terapis')?>" method="post">
                                                    <input type="hidden" name="id_terapis" value="<?php print $terapis[0]['id_terapis']?>">
                                                    <td>
                                                    <input type="text" class = "form-control" value ="<?php print $terapis[0]['nama_terapis']?>" name="nama_terapis" id = "nama_terapis">
                                                    </td>
                                                    <td>
                                                    <select type="button"  name = "jam_mulai" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <div class="dropdown-menu">
                                                            <option class="dropdown-item" value="0">Pilih Jam Mulai</option>
                                                            <option class="dropdown-item" value="09:30:00" <?php if($terapis[0]['jam_mulai']=="09:30:00") echo 'selected="selected"'; ?>>09:30</option>
                                                            <option class="dropdown-item" value="10:00:00" <?php if($terapis[0]['jam_mulai']=="10:00:00") echo 'selected="selected"'; ?>>10:00</option>
                                                            <option class="dropdown-item" value="11:00:00" <?php if($terapis[0]['jam_mulai']=="11:00:00") echo 'selected="selected"'; ?>>11:00</option>
                                                        </div>
                                                    </select>
                                                    </td>
                                                    <td>
                                                    <select type="button" name = "jam_selesai" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <div class="dropdown-menu">
                                                            <option class="dropdown-item" value="0">Pilih Jam Selesai</option>
                                                            <option class="dropdown-item" value="18:00:00" <?php if($terapis[0]['jam_selesai']=="18:00:00") echo 'selected="selected"'; ?>>18:00</option>
                                                            <option class="dropdown-item" value="19:00:00"  <?php if($terapis[0]['jam_selesai']=="19:00:00") echo 'selected="selected"'; ?>>19:00</option>
                                                            <option class="dropdown-item" value="20:00:00"  <?php if($terapis[0]['jam_selesai']=="20:00:00") echo 'selected="selected"'; ?>>20:00</option>
                                                        </div>
                                                    </select>
                                                    </td>
                                                    <td>
                                                        <button type ="submit" id="button_submit_terapis" class = "btn btn-outline-danger" name="button_submit_terapis">Submit</button>
                                                    </td>
                                                </form>
                                                </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
