<?php
require '../../config/connection.php';
include '../../model/user.php';
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<?php
    include $_SERVER['DOCUMENT_ROOT']."/sherly/view/template/head.php";
?>
<body class = "app sidebar-fixed aside-menu-off-canvas aside-menu-hidden header-fixed">
    <?php
        include $_SERVER['DOCUMENT_ROOT']."/sherly/view/template/header.php";
    ?>
    <div class="app-body">
        <?php
        include $_SERVER['DOCUMENT_ROOT']."/sherly/view/template/sidebar_admin.php";
        ?>
        <main class = "main">
            <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                <li class="breadcrumb-item ">
                    <a href="">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">Dashboards</a>
                </li>
                <li class="breadcrumb-item active">Dashboard Property</li>
            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <div class="h5 text-dark">SMS Gateway</div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Pilih Kategori Pelanggan
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#">Pilih Kategori Pelanggan</a>
                                                <a class="dropdown-item" href="#">Gold</a>
                                                <a class="dropdown-item" href="#">Silver</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="table-responsive">
                                                <table class="table color-table danger-table">
                                                    <th>ID</th>
                                                    <th>Nama Pelanggan</th>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Lili Agustian</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group row">
                                                    <textarea id="textarea-input" name="textarea-input" rows="9" class="form-control" placeholder="Content.."></textarea>
                                                    <button type="button" class="btn btn-outline-danger">Kirim</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <?php
        include $_SERVER['DOCUMENT_ROOT']."/sherly/view/template/footer.php";
    ?>
</body>
</html>