
        <main class = "main">
            <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                <li class="breadcrumb-item ">
                    <a href="">Transaksi</a>
                </li>
                <li class="breadcrumb-item active">Transaksi</li>
            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <div class="h5 text-dark">Data Transaksi Pelanggan</div>
                                            <a href="transaksi/get_kmeans_result" class = "btn btn-danger">Proses Cluster</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="table-responsive">
                                                <table class="table color-table danger-table">
                                                    <th>Nama Pelanggan</th>
                                                    <th> Total Kedatangan </th>
                                                    <th> Jumlah Bayar </th>
                                                    <th> Cluster </th>
                                                    <th> Kategori </th>
                                                    <?php
                                                        $i=0;
                                                        foreach ($kmeans as $d) {
                                                            $i++;
                                                    ?>
                                                    <tr>
                                                        <td><?= $d['username']?></td>
                                                        <td><?= $d['total_dtg']?></td>
                                                        <td><?= $d['total_byr']?> </td>
                                                        <td><?php if(isset($d['cluster']) && $d['cluster'] > 0) echo $d['cluster'] ?></td>
                                                        <td><?php if(isset($d['kategori'])) echo $d['kategori'] ?></td>
                                                    </tr>
                                                        <?php } ?>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="card">
                                                <div class="card-body">
                                                    <form action="#" method="get">
                                                        <h5 class="card-title">Proses Transaksi</h5>
                                                        Jenis Pelanggan
                                                        <select name="jenis_pelanggan" class = "btn btn-outline-danger dropdown-toggle" id="jenis_pelanggan">
                                                            <option value="1">Seluruh Data</option>
                                                            <option value="2">Silver</option>
                                                            <option value="3">Gold</option>
                                                            <option value="4">Platinum</option>
                                                        </select>
                                                        <button type="submit" class="btn btn-danger">Tampilkan</button>
                                                    </form>
                                                    <!-- <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        All Data
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="#">All Data</a>
                                                        <a class="dropdown-item" href="#">Bulan 1</a>
                                                        <a class="dropdown-item" href="#">Bulan 2</a>
                                                    </div> -->
                                                </div>
                                            </div>
                                        </div>
                                        <!-- </form> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
