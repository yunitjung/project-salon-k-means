
        <main class = "main">
            <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                <li class="breadcrumb-item ">
                    <a href="">Data Pelanggan</a>
                </li>
                <li class="breadcrumb-item active">Update Data Pelanggan</li>
            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <div class="h5 text-dark">Update User</div>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table color-table danger-table">
                                                <th> Username</th>
                                                <th> Nama </th>
                                                <th> Password </th>
                                                <th> Email </th>
                                                <th> No hp </th>
                                                <tr>
                                                    <form id= "update_pelanggan" action="<?= base_url('pelanggan/update_pelanggan')?>" method="post">
                                                        <td><?php print  $pelanggan->username?><input type = "hidden" name="username" id="username" value=<?php print  $pelanggan->username?>></td>
                                                        <td>
                                                        <input type="text" class = "form-control" value ="<?php print $pelanggan->name?>" name="name" id = "name" >
                                                        </td>
                                                        <td>
                                                        <input type="password" class = "form-control" value ="<?php print $pelanggan->password?>" name="password" id = "password" >
                                                        </td>
                                                        <td>
                                                        <input type="email" class = "form-control" value ="<?php print $pelanggan->email?>" name="email" id = "email" >
                                                        </td>
                                                        <td>
                                                        <input type="text" class = "form-control" value =<?php print $pelanggan->no_hp?> name="no_hp" id = "no_hp" >
                                                        </td>
                                                        <td>
                                                            <button type ="submit" id="button_submit_pelanggan" class = "btn btn-outline-danger" name="button_submit_pelanggan"> Submit</button>
                                                        </td>
                                                    </form>
                                                </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
