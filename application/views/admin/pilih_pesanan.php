
        <main class = "main">
            <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                <li class="breadcrumb-item ">
                    <a href="">Pemesanan</a>
                </li>
                <li class="breadcrumb-item active">Invoice</li>
            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <div class="h5 text-dark">Pilih Pesanan</div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class = "table-responsive">
                                            <table class="table table-bordered color-table danger-table">
                                                <tr><th>Id Pemesanan</th><td><?php echo $pemesanan->id_pemesanan?></td></tr>
                                                <tr><th>Tanggal</th><td><?php echo $pemesanan->tanggal ?></td></tr>
                                                <tr><th>Nama</th><td><?php echo $pemesanan->name ?> </td></tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                            <div class="alert alert-danger" <?php if ($this->session->flashdata('insert_detail') == 1 || $this->session->flashdata('insert_detail') == '' ) echo 'hidden';   if ($this->session->flashdata('insert_detail') == 0) echo ''; ?>>
                                                Durasi terlalu lama ! Pilih perawatan/terapis lain
                                            </div>
                                        </div>
                                    <div class="table-responsive">
                                        <table class="table color-table danger-table">

                                                <th> Nama Perawatan</th>
                                                <th> Nama Terapis </th>
                                                <th> Durasi </th>
                                                <th> Harga </th>
                                                <?php if(!$data['status']) {?>
                                                <th> Aksi </th>
                                                <?php } ?>
                                                <tr>
                                                <?php if(!$data['status']) {?>
                                                <?php
                                                }
                                                    $data_detail = $controller->view_detail_trx($id_pemesanan);
                                                    if(!empty($data_detail)){
                                                ?>
                                                <?php foreach($data_detail as $dd){ ?>
                                                <tr>
                                                    <input type="hidden" name="id_detail" id="id_detail" value = "<?php echo $dd['id_detail'] ?>">
                                                    <td><?php echo $dd['nama_perawatan']?></td>
                                                    <td><?php
                                                    if(!$dd['id_terapis'])
                                                    { ?>
                                                    <select type="button"  name = "id_terapis" id="dropdown_terapis" class="btn btn-danger dropdown-toggle">
                                                        <div class="dropdown-menu">
                                                            <option class="dropdown-item" value="0">Pilih Terapis</option>
                                                        <?php
                                                        $data_t = $terapis_c->view_terapis();
                                                        foreach($data_t as $dt){ ?>
                                                            <option class="dropdown-item" value="<?php echo $dt['id_terapis']?>"><?php echo $dt['nama_terapis']?></option>
                                                        <?php
                                                        }?>
                                                        </div>
                                                    </select>
                                                    <?php }
                                                    else echo $dd['nama_terapis']?></td>
                                                    <td><?php echo $dd['durasi']?></td>
                                                    <td><?php echo $dd['harga']?></td>
                                                    <?php if(!$data['status']) {?>
                                                    <td>
                                                        <a href="" data-toggle="tooltip" data-original-title="Edit" id="update_terapis_again" name="update_terapis_again">
                                                            <i class="fa fa-pencil text-info"></i>
                                                        </a>
                                                    </td>
                                                    <?php }?>
                                                </tr>
                                                <?php }  }?>
                                                <?php if(!$data['status']) {?>
                                                <tr>
                                                    <td colspan=5 class="text-right">
                                                        <a href="index.php?page=pemesanan_controller&func=update_pemesanan&id=<?php echo $id_pemesanan?>" class = "btn btn-outline-danger">Finish</a>
                                                    </td>
                                                </tr>
                                                <?php }
                                                else { ?>
                                                <tr>
                                                    <td colspan=2>
                                                        <strong>Total</strong>
                                                    </td>
                                                    <td>
                                                        <strong><?php echo $data['total_durasi'] ?></strong>
                                                    </td>
                                                    <td>
                                                        <strong><?php echo $data['total_bayar'] ?></strong>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <?php
        include $_SERVER['DOCUMENT_ROOT']."/sherly/view/template/footer.php";
    ?>
</body>
</html>