<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Signup</title>

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url()?>/assets/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('assets/img/logo.jpg')?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets/img/logo.jpg')?>">
    <link rel="manifest" href="<?php echo base_url()?>/assets/img/favicon/manifest.json">
    <link rel="mask-icon" href="<?php echo base_url()?>/assets/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">

    <!-- fonts -->
    <link rel="stylesheet" href="<?php echo base_url()?>/assets/fonts/md-fonts/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>/assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="<?php echo base_url()?>/assets/libs/animate.css/animate.min.css">

     <!-- jquery-loading -->
     <link rel="stylesheet" href="<?php echo base_url()?>/assets/libs/jquery-loading/dist/jquery.loading.min.css">
    <!-- octadmin main style -->
    <link id="pageStyle" rel="stylesheet" href="<?php echo base_url()?>/assets/css/style-google-plus.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script>var base_url = '<?php echo base_url() ?>';</script>
</head>
<body>
<section class="container-pages">

<div class="brand-logo float-left"><img src="<?= base_url('assets/img/logo.jpg') ?>" alt="Logo STAR" width="200px"></div>

<!-- <div class="pages-tag-line text-white">
    <div class="h4">Let's Get Started .!</div>
    <small> most powerfull most selling Admin Panel In The World</small>
</div> -->

<div class="card pages-card col-lg-4 col-md-6 col-sm-6">
    <div class="card-body ">
        <div class="h4 text-center text-theme"><strong>Signup</strong></div>
        <div class="small text-center text-dark"> Create an Account </div>
             <form action="<?= base_url('pelanggan/insert_user') ?>" method="post">
                 <div class="form-group">
                     <div class="input-group">
                         <span class="input-group-addon text-theme"><i class="fa fa-user"></i>
                         </span>
                         <input type="text" id="name" name="name" class="form-control" placeholder="Name">
                     </div>
                 </div>
                <div class="form-group">
                    <div class="input-group">
                         <span class="input-group-addon text-theme"><i class="fa fa-user"></i>
                        </span>
                        <input type="text" id="username" name="username" class="form-control" placeholder="Username">
                    </div>
                </div>
                <div class="form-group">
                     <div class="input-group">
                         <span class="input-group-addon text-theme"><i class="fa fa-mobile-phone"></i>
                         </span>
                         <input type="text" id="no_hp" name="no_hp" class="form-control" maxlength=13 placeholder="No Handphone">
                     </div>
                 </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon text-theme"><i class="fa fa-envelope"></i>
                                    </span>
                        <input type="email" id="email" name="email" class="form-control" placeholder="Email">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon text-theme"><i class="fa fa-asterisk"></i></span>
                        <input type="password" id="password" name="password" class="form-control" maxlength = 8 placeholder="Password">

                    </div>
                </div>
                <div class="form-group form-actions">
                    <button type="submit" class="btn  btn-theme login-btn ">   Signup   </button>
                </div>
            </form>
            <!-- end form -->
            <div class="text-center">
                <small>Sudah punya akun?
                    <a href="login" class="text-theme">Login disini</a>
                </small>
            </div>

    </div>
    <!-- end card-body -->
</div>
<!-- end card -->
</section>

    <!-- end section container -->

    <footer class="app-footer">
        <a href="#" class="text-theme">octAdmin</a> &copy; 2018 octaThemes.
    </footer>

    <!-- Bootstrap and necessary plugins -->
    <script src="<?php echo base_url()?>assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo base_url()?>assets/libs/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/libs/PACE/pace.min.js"></script>
    <script src="<?php echo base_url()?>assets/libs/chart.js/dist/Chart.min.js"></script>
    <script src="<?php echo base_url()?>assets/libs/nicescroll/jquery.nicescroll.min.js"></script>

     <!--Flot Chart -->
    <script src="<?php echo base_url()?>assets/libs/charts-flot/jquery.flot.js"></script>
    <script src="<?php echo base_url()?>assets/libs/charts-flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url()?>assets/libs/charts-flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url()?>assets/libs/charts-flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url()?>assets/libs/charts-flot/jquery.flot.crosshair.js"></script>


    <!--morris js -->
    <script src="<?php echo base_url()?>assets/libs/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url()?>assets/libs/charts-morris-chart/morris.min.js"></script>

       <!-- jquery-loading -->
    <script src="<?php echo base_url()?>assets/libs/jquery-loading/dist/jquery.loading.min.js"></script>

    <!-- octadmin Main Script -->
    <script src="<?php echo base_url()?>assets/js/app.js"></script>

    <!-- dashboard-property-examples -->
    <script src="<?php echo base_url()?>assets/js/dashboard-property-examples.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="<?php echo base_url()?>assets/js/admin.js"></script>
    <script src = "<?php echo base_url()?>assets/js/engine.js"></script>

    </body>
</html>