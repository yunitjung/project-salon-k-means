$(function(){
    var btn_bayar = $('#pesan_pelanggan').find('#btn_bayar'),
        form_pesan = $('#pesan_pelanggan').find('#form_bayar'),
        btn_submit_terapis = $('#update_terapis'),
        btn_update_status = $('#btn_update_status'),
        btn_pesan_pembeli = $('.btn_pesan_pembeli'),
        keranjang_id_perawatan = $('#daftar_login').find('#id_perawatan'),
        btn_voucher= $('#btn_voucher');


        btn_bayar.on('click', function(){
            $this = $(this),
            id_pemesanan = $this.data('id');
            data = form_pesan.serializeArray();
            $.ajax({
                url : base_url+'pemesanan/ajax_update_pesanan',
                data : data,
                type : 'POST'
            })
            .done(function(res){
                res = JSON.parse(res);
                if(res.status)
                {
                    window.location.href = base_url+'pemesanan/invoice/'+id_pemesanan;
                }
                else
                {
                    window.location.reload();
                }
            });
        });

        btn_submit_terapis.on('click', function(){
            $this = $(this),
            id_pemesanan = $this.data('id'),
            id_detail = $this.data('id_detail');
            id_terapis = $('select[name="id_terapis"]').val();
            $.ajax({
                url : base_url+'pemesanan/ajax_update_detail',
                data: {
                    'id_detail_trx' : id_detail,
                    'id_terapis'    : id_terapis
                },
                type:'POST'
            })
            .done(function(res){
                window.location.reload();
            });
        });

        btn_pesan_pembeli.on('click', function(){
            $this= $(this),
            id_perawatan = $this.data('id');
            keranjang_id_perawatan.val(id_perawatan);
        });

        btn_voucher.click(function(){
            var form_voucher = $('form#form_voucher'),
                data = form_voucher.serializeArray();
            $.ajax({
                url : base_url+'pemesanan/check_voucher',
                type : 'post',
                data : data,
                success:function(data)
                {
                    $('#label-alert').html('Voucher sudah diredeem! ');
                    $('button#btn_redeem_voucher').hide();
                    $('#btn_close_voucher').trigger('click');
                    // $('#modal_voucher').modal('hide');
                }
            });


        });
});